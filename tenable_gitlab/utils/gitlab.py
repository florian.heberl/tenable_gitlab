import logging
import os
import re
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple

from gitlab.client import Gitlab
from gitlab.v4.objects.issues import ProjectIssue, ProjectIssueLink
from jinja2 import Template

logger = logging.getLogger(__name__)

CURRENT_FILE_PATH = os.path.abspath(os.path.dirname(__file__))


def loadJinjaTemplate(path: str) -> Template:
    return Template(open(path).read())


class GitlabUtils:
    def __init__(
        self,
        gitlab_client: Gitlab,
        project_id: int,
        incident_template_path: str,
        plugin_issue_template_path: str,
        default_incident_labels: List[str],
        default_plugin_issue_labels: List[str],
        confidential_issues: bool = True,
    ):
        """Utilities to interact wtih gitlab

        Args:
            gitlab_client (Gitlab): GitLab client from python-gitlab library
            project_id (int): The project id on gitlab where everything is created
             incident_template_path (str): Jinja template that will be used as the description on incidents. Defaults to None.
            plugin_issue_template_path (str): Jinja template that will be used as the description on plugin issues. Defaults to None.
            incident_labels (str): Comma separated list of labels that will be assigned to incidents. Defaults to None.
            plugin_issue_labels (str): Comma separated list of labels that will be assigned to plugin issues. Defaults to None.
            confidential_issues (bool, optional): Wether issues should be marked as confidential. Defaults to True.
        """
        self._gitlab_client = gitlab_client
        self._project = self._gitlab_client.projects.get(project_id)
        self._confidential_issues = confidential_issues

        self._plugin_issue_template: Template = loadJinjaTemplate(
            plugin_issue_template_path
        )

        self._incident_template: Template = loadJinjaTemplate(
            incident_template_path
        )

        self._default_plugin_issue_labels = default_plugin_issue_labels
        self._default_incident_labels = default_incident_labels

    def create_or_update_incident(
        self,
        plugin: Dict[str, str],
        asset: Dict[str, str],
        vulnerability: Dict[str, str],
        extra_labels: List[str] = [],
    ) -> ProjectIssue:
        """Open an incident on gitlab for a vulnerability on tenable.io.
        Creates an issue for the plugin if it dosen't exist.
        Creates a Link in gitlab between he incident and the plugin issue.

        Args:
            plugin (Dict[str, str]): Details for the plugin
            asset (Dict[str, str]): Details for the asset
            vulnerability (Dict[str, str]): Details for the vulnerability
            extra_labels (List[str]): Added when creating the incident
        Returns:
            ProjectIssue: Created incident
        """
        # Get the plugin issue or create if it doesn't exist
        plugin_issue = self.create_or_update_plugin_issue(plugin)

        plugin_id = int(plugin["id"])
        plugin_name = plugin["name"]
        hostname = self._find_asset_hostname(asset)

        title = f"Host: {hostname} | {plugin_name}"

        description = self._incident_template.render(
            plugin=plugin, asset=asset, vulnerability=vulnerability
        )
        attributes: Dict[str, Any] = {
            "title": title,
            "description": description,
            "confidential": str(self._confidential_issues),
            "issue_type": "incident",
        }

        labels = self._default_incident_labels
        # Label issue with the correct severity number
        labels.append(f"severity::{vulnerability['severity_num']}")
        labels.extend(extra_labels)

        # Update the issue if it already exists
        plugin_id = int(plugin["id"])
        existing_issue = self.get_asset_incident(plugin_id, hostname)
        if existing_issue:
            # TODO: Don't update when we don't need to
            existing_issue.title = attributes["title"]
            existing_issue.description = attributes["description"]
            existing_issue.labels.extend(labels)
            existing_issue.save()
            logger.info(
                f"Updated Incident with id: {existing_issue.get_id()} on GitLab for plugin with id: {plugin_id} and asset with hostname: {hostname}"
            )

            return existing_issue

        attributes["labels"] = labels
        asset_incident = self._project.issues.create(attributes)
        logger.info(
            f"Created a new Incident with id: {asset_incident.get_id()} on GitLab for plugin with id: {plugin_id} and asset with hostname: {hostname}"
        )

        self._link_plugin_issue_to_incident(plugin_issue, asset_incident)

        return asset_incident

    def auto_close_updated_before(
        self,
        updated_before: int,
        extra_labels: List[str] = [],
    ) -> None:
        """This function will automatically close issues that were not updated after the given date.
        This is useful to cleanup incidents for assets that stopped being tracked in Tenable.io.

        Args:
            updated_before(int): The date in milliseconds to check for.
        """
        iso_8601_date = datetime.fromtimestamp(updated_before).strftime(
            "%Y-%m-%dT%H:%M:%S%z"
        )

        logger.info(
            f"Searching and closing incidents in GitLab that were not updated since {iso_8601_date}."
        )

        issues = self._project.issues.list(
            updated_before=iso_8601_date,
            labels=self._default_incident_labels,
            state="opened",
            sort="desc",
            iterator=True,
        )
        for i in issues:
            logger.info(
                f"Closing Incident with id: {i.get_id()} on GitLab with title: {i.title}. The asset wasnt updated since {i.updated_at} and is no longer being tracked by Tenable."
            )
            i.labels.extend(extra_labels)
            i.state_event = "close"
            i.discussions.create(
                {
                    "body": f"Closing because the asset wasnt updated since {i.updated_at} and is no longer being tracked by Tenable."
                }
            )
            i.save()

            # get the plugin_id from linked_issues
            linked_issues = i.links.list()
            for linked_issue in linked_issues:
                title = linked_issue.title
                match = re.match(r"Nessus Plugin: (\d+) .*", title)
                if match:
                    plugin_id = match.groups()[0]
                    self._close_plugin_if_no_open_vulnerabilities(plugin_id)
        return None

    def close_incident(
        self,
        plugin: Dict[str, str],
        asset: Dict[str, str],
        vulnerability: Dict[str, str],
        reason: str,
        extra_labels: List[str] = [],
    ) -> Optional[ProjectIssue]:
        """Close an incident on gitlab for a vulnerability fixed on tenable.io.
        Closes the plugin issue if there are no more linked incidents

        Args:
            plugin (Dict[str, str]): Details for the plugin
            asset (Dict[str, str]): Details for the asset
            vulnerability (Dict[str, str]): Details for the vulnerability
            reason (str): Reason why this is being closed. A comment will be left with the reason.
            extra_labels (List[str]): Added when closing the incident
        Returns:
            ProjectIssue: Created incident
        """
        # Get the plugin issue or create if it doesn't exist
        # plugin_issue = self.create_or_update_plugin_issue(plugin)

        plugin_id = int(plugin["id"])
        plugin_name = plugin["name"]
        hostname = self._find_asset_hostname(asset)

        title = f"Host: {hostname} | {plugin_name}"

        description = self._incident_template.render(
            plugin=plugin, asset=asset, vulnerability=vulnerability
        )
        attributes: Dict[str, Any] = {
            "title": title,
            "description": description,
            "confidential": str(self._confidential_issues),
            "issue_type": "incident",
        }

        labels = self._default_incident_labels
        # Label issue with the correct severity number
        labels.append(f"severity::{vulnerability['severity_num']}")
        labels.extend(extra_labels)

        # Update the issue if it already exists
        plugin_id = int(plugin["id"])
        existing_issue = self.get_asset_incident(plugin_id, hostname)
        if existing_issue:
            existing_issue.title = attributes["title"]
            existing_issue.description = attributes["description"]
            existing_issue.labels.extend(labels)
            existing_issue.state_event = "close"
            existing_issue.discussions.create({"body": reason})
            existing_issue.save()
            logger.info(
                f"Closing Incident with id: {existing_issue.get_id()} on GitLab for plugin with id: {plugin_id} and asset with hostname: {hostname}"
            )
            self._close_plugin_if_no_open_vulnerabilities(plugin_id)
            return existing_issue
        else:
            logger.info(
                f"Could not find an open incident on GitLab for plugin with id: {plugin_id} and asset with hostname: {hostname}"
            )
        return None

    def create_or_update_plugin_issue(
        self, plugin: Dict[str, str]
    ) -> ProjectIssue:
        """Create an issue on gitlab with the details of the plugin.
        If an open issue exists, updates that issue.

        Args:
            plugin (Dict[str, str]): Details for the plugin

        Returns:
            ProjectIssue: The issue created
        """
        plugin_id = int(plugin["id"])
        plugin_name = plugin["name"]

        title = f"Nessus Plugin: {plugin_id} | {plugin_name}"

        description = self._plugin_issue_template.render(plugin=plugin)

        attributes: Dict[str, Any] = {
            "title": title,
            "description": description,
            "confidential": str(self._confidential_issues),
            "issue_type": "issue",
        }
        labels = self._default_plugin_issue_labels

        existing_issue = self.get_plugin_issue(plugin_id)

        if existing_issue:
            existing_issue.title = attributes["title"]
            existing_issue.description = attributes["description"]
            existing_issue.save()
            return existing_issue

        attributes["labels"] = labels
        issue = self._project.issues.create(attributes)
        logger.info(
            f"Created a new Issue with id: {issue.get_id()} on GitLab for plugin with id: {plugin_id}"
        )
        return issue

    def get_plugin_issue(self, plugin_id: int) -> Optional[ProjectIssue]:
        """Get the opened issue for a given plugin_id.
        If there is more than one, returns the most recent.

        Args:
            plugin_id (int): The id of the plugin

        Returns:
            Optional[ProjectIssue]: The issue
        """
        logger.debug(f"Getting issue for plugin {plugin_id}")
        issues = self._project.issues.list(
            search=plugin_id,
            labels=self._default_plugin_issue_labels,
            state="opened",
            sort="desc",
        )

        logger.debug(f"Found issues for plugin {plugin_id}: issues")
        if len(issues) == 0:
            return None

        return issues[0]

    def _get_open_vulnerability_links_for_plugin(
        self, plugin_id: int
    ) -> List[ProjectIssueLink]:
        plugin_issue = self.get_plugin_issue(plugin_id)
        if plugin_issue is None:
            logger.debug(
                f"No Issue found for plugin {plugin_id}.",
            )
            return []

        linked_asset_vulnerabilities = plugin_issue.links.list()
        res = []
        # Remove non "opened". Can't filter this in the API.
        for link in linked_asset_vulnerabilities.copy():
            if "opened" in link.state:
                res.append(link)
        return res

    def get_asset_incident(
        self, plugin_id: int, asset_hostname: str
    ) -> Optional[ProjectIssue]:
        """Get the opened incident associated with a plugin for a given asset.

        Args:
            plugin_id (int): The id of the plugin
            asset_hostname (str): The hostname of the asset

        Returns:
            Optional[ProjectIssue]: The issue
        """

        logger.debug(
            f"Getting incident for plugin {plugin_id} and asset {asset_hostname}:",
        )

        linked_asset_vulnerabilities = (
            self._get_open_vulnerability_links_for_plugin(plugin_id)
        )

        for link in linked_asset_vulnerabilities:
            if "opened" in link.state and asset_hostname in link.title:
                issue = self._project.issues.get(link.iid)
                logger.debug(
                    f"Getting incident for plugin {plugin_id} and asset {asset_hostname}:",
                )
                return issue

        logger.debug(
            f"Found issue for {plugin_id} but no incident open for {asset_hostname}:",
        )
        return None

    def _link_plugin_issue_to_incident(
        self, plugin_issue: ProjectIssue, asset_incident: ProjectIssue
    ) -> Tuple[ProjectIssueLink, ProjectIssueLink]:
        data = {
            "target_project_id": plugin_issue.project_id,
            "target_issue_iid": plugin_issue.iid,
        }
        return asset_incident.links.create(data)

    def _find_asset_hostname(self, asset: Dict[str, Any]) -> str:
        # hostname or fqdn are not guaranteed to exist in tenable.io
        hostname = asset["ipv4"][0]
        if len(asset["hostname"]):
            # hostname could be either a list or a str
            if isinstance(asset["hostname"], str):
                hostname = asset["hostname"]
            else:
                hostname = asset["hostname"][0]
        elif len(asset["fqdn"]):
            hostname = asset["fqdn"][0]
        return hostname

    def _close_plugin_if_no_open_vulnerabilities(self, plugin_id: int) -> bool:
        plugin_issue = self.get_plugin_issue(plugin_id)
        if plugin_issue:
            links = self._get_open_vulnerability_links_for_plugin(plugin_id)
            if len(links) == 0:
                logger.info(
                    f"Closing plugin issue {plugin_issue.get_id()} because there are no more vulnerabilities for this plugin."
                )
                plugin_issue.state_event = "close"
                plugin_issue.discussions.create(
                    {
                        "body": "Closing this issue because there are no more open vulnerabilities."
                    }
                )
                plugin_issue.save()
                return True
        return False
