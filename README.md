# TenableGitLab

This integration pulls vulnerability data from Tenable.io and creates Issues and Incidents on GitLab.com.

We are using this library in GitLab to help us maintain our [vulnerability management program](https://about.gitlab.com/handbook/engineering/security/vulnerability_management)

## Features

- [x] Open an Issue on Gitlab.com for every plugin with an open vulnerability
- [x] Open an Incident on Gitlab.com for each vulnerability found and create a link with the plugin issue.
- [x] Close Incidents for terminated and deleted assets
- [x] Close Incidents for remediated vulnerabilities
- [x] Close the Issue created for the plugin when there are no new vulnerabilities are open
- [x] Close Incident for vulnerabilities that were [recasted](https://docs.tenable.com/tenableio/Content/Settings/AboutRecastRules.htm) as lower severity

## Install

On `requirements.txt`:

```
--extra-index-url https://gitlab.com/api/v4/projects/27868144/packages/pypi/simple
tenable-gitlab==0.0.1

...

```

or

```
pip install tenable-gitlab --extra-index-url https://gitlab.com/api/v4/projects/27868144/packages/pypi/simple
```

## Usage

```python
import logging

from gitlab import Gitlab
from tenable.io import TenableIO

from tenable_gitlab.tenable2gitlab import Tenable2Gitlab
from tenable_gitlab.config import Config, Severity

loglevel = logging.INFO

logging.basicConfig(level=loglevel)

TENABLE_ACCESS_KEY = "xxxx"
TENABLE_SECRET_KEY = "xxxx"

GITLAB_TOKEN = "xxx"
GITLAB_PROJECT_ID = 123

if __name__ == "__main__":
    tenable_client = TenableIO(
        access_key = TENABLE_ACCESS_KEY,
        secret_key = TENABLE_SECRET_KEY,
    )
    gitlab_client = Gitlab("https://gitlab.com", GITLAB_TOKEN)

    config = Config()
    config.minimum_severity = Severity.CRITICAL

    t2g = Tenable2Gitlab.create_from_clients(tenable_client, gitlab_client, GITLAB_PROJECT_ID, config)
    t2g.sync()
```

## Development

Requirements:

- python
- python-pip
- virtualenv

Setup:

1. Create a python virtual environment

```bash
virtualenv .venv/
```

2. Activate the virtual environment

```bash
source .venv/bin/activate
```

3. Install all python dependencies necessary for you to be able to run tests

```bash
pip install -r test-requirements.txt
```

4. Run the unit tests

```bash
pytest test/unit/
```

5. Run the integration tests

```bash
GITLAB_PRIVATE_TOKEN="xxx" pytest test/integration
```
