from unittest.mock import MagicMock

import pytest
from gitlab.client import Gitlab
from gitlab.v4.objects import Project, ProjectManager
from pytest_mock import MockerFixture
from tenable.io import TenableIO

from tenable_gitlab.config import Config
from tenable_gitlab.utils.gitlab import GitlabUtils
from tenable_gitlab.utils.tenable import TenableUtils


@pytest.fixture(scope="module")
def dummy_gitlab_client() -> Gitlab:
    return Gitlab("https://example.com")


@pytest.fixture
def dummy_gitlab_utils(
    mocker: MockerFixture, dummy_gitlab_client: Gitlab
) -> GitlabUtils:
    pm = ProjectManager(dummy_gitlab_client)
    mocker.patch(
        "gitlab.v4.objects.projects.ProjectManager.get",
        return_value=Project(pm, {}),
    )

    config = Config()
    gu = GitlabUtils(
        dummy_gitlab_client,
        0,
        config.incident_template_path,
        config.plugin_issue_template_path,
        config.default_incident_labels,
        config.default_plugin_labels,
    )
    return gu


@pytest.fixture(scope="module")
def dummy_tenable_client() -> Gitlab:
    return TenableIO(access_key="x", secret_key="x")


@pytest.fixture
def dummy_tenable_utils(dummy_tenable_client: TenableIO) -> TenableUtils:
    return TenableUtils(dummy_tenable_client)


@pytest.fixture
def mocked_get_asset_details(
    mocker: MockerFixture,
) -> MagicMock:
    return mocker.patch(
        "tenable_gitlab.utils.tenable.TenableUtils.get_asset_details",
        return_value={},
    )
