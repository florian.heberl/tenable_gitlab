from unittest.mock import MagicMock

import pytest
from gitlab.client import Gitlab
from gitlab.v4.objects import ProjectManager
from gitlab.v4.objects.issues import ProjectIssue
from gitlab.v4.objects.projects import Project
from pytest_mock import MockerFixture


@pytest.fixture(scope="module")
def dummy_gitlab_client() -> Gitlab:
    return Gitlab("https://example.com")


@pytest.fixture
def dummy_gitlab_project_manager(
    mocker: MockerFixture, dummy_gitlab_client: Gitlab
) -> ProjectManager:
    pm = ProjectManager(dummy_gitlab_client)
    mocker.patch(
        "gitlab.v4.objects.projects.ProjectManager.get",
        return_value=Project(pm, {}),
    )
    return pm


@pytest.fixture
def mocked_gitlab_utils_get_plugin_issue(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "tenable_gitlab.utils.gitlab.GitlabUtils.get_plugin_issue",
        return_value=None,
    )


@pytest.fixture
def mocked_gitlab_utils__get_open_vulnerability_links_for_plugin(
    mocker: MockerFixture,
) -> MagicMock:
    return mocker.patch(
        "tenable_gitlab.utils.gitlab.GitlabUtils._get_open_vulnerability_links_for_plugin",
        return_value=[],
    )


@pytest.fixture
def mocked_gitlab_project_issue_manager_create(
    mocker: MockerFixture,
    dummy_gitlab_project_manager: ProjectManager,
) -> MagicMock:
    return mocker.patch(
        "gitlab.v4.objects.issues.ProjectIssueManager.create",
        return_value=ProjectIssue(
            dummy_gitlab_project_manager, {"project_id": 0, "iid": 0}
        ),
    )


@pytest.fixture
def mocked_gitlab_project_issue_manager_get(
    mocker: MockerFixture,
    dummy_gitlab_project_manager: ProjectManager,
) -> MagicMock:
    return mocker.patch(
        "gitlab.v4.objects.issues.ProjectIssueManager.get",
        return_value=ProjectIssue(
            dummy_gitlab_project_manager, {"project_id": 0, "iid": 0}
        ),
    )


@pytest.fixture
def mocked_gitlab_project_link_issue_manager_create(
    mocker: MockerFixture,
    dummy_gitlab_project_manager: ProjectManager,
) -> MagicMock:
    return mocker.patch(
        "gitlab.v4.objects.issues.ProjectIssueLinkManager.create",
        return_value=None,
    )
