from test.conftest import (
    generate_random_asset,
    generate_random_plugin,
    generate_random_severity,
    generate_random_vulnerability,
    random_string,
)
from typing import Any, Dict
from unittest.mock import MagicMock

from pytest_mock import MockFixture

from tenable_gitlab.config import Config, Severity
from tenable_gitlab.tenable2gitlab import Tenable2Gitlab
from tenable_gitlab.utils.gitlab import GitlabUtils
from tenable_gitlab.utils.tenable import TenableUtils


def build_dummy_vulnerabity() -> Dict[str, Any]:
    sev = generate_random_severity()
    vuln = generate_random_vulnerability(sev)
    vuln["plugin"] = generate_random_plugin()
    vuln["asset"] = generate_random_asset()
    return vuln


class TestSyncOpen:
    def test_sync_open(
        self,
        mocker: MockFixture,
        dummy_tenable_utils: TenableUtils,
        dummy_gitlab_utils: GitlabUtils,
        mocked_get_asset_details: MagicMock,
    ) -> None:
        t2g = Tenable2Gitlab(dummy_tenable_utils, dummy_gitlab_utils)
        dummy_vuln = build_dummy_vulnerabity()

        # the original object is altered in runtime.
        # better for efficiency, bad for tsts
        test_vuln = dummy_vuln.copy()

        mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.open_vulnerabilities_iterator",
            return_value=[dummy_vuln],
        )

        mocked_create_or_update_incident = mocker.patch(
            "tenable_gitlab.utils.gitlab.GitlabUtils.create_or_update_incident",
            return_value={},
        )

        t2g.sync_open_issues()

        asset = test_vuln.pop("asset")
        plugin = test_vuln.pop("plugin")

        mocked_get_asset_details.assert_called_once_with(asset["uuid"])
        mocked_create_or_update_incident.assert_called_once_with(
            plugin, asset, test_vuln, Config.open_incident_labels
        )

    def test_sync_open_many_vulns(
        self,
        mocker: MockFixture,
        dummy_tenable_utils: TenableUtils,
        dummy_gitlab_utils: GitlabUtils,
        mocked_get_asset_details: MagicMock,
    ) -> None:
        t2g = Tenable2Gitlab(dummy_tenable_utils, dummy_gitlab_utils)

        total_vuln_count = 10
        dummy_vuln_list = [
            build_dummy_vulnerabity() for i in range(total_vuln_count)
        ]

        # the original object is altered in runtime.
        # better for efficiency, bad for tsts
        test_vuln = dummy_vuln_list[-1].copy()

        mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.open_vulnerabilities_iterator",
            return_value=dummy_vuln_list,
        )
        mocked_get_asset_details = mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.get_asset_details",
            return_value={},
        )

        mocked_create_or_update_incident = mocker.patch(
            "tenable_gitlab.utils.gitlab.GitlabUtils.create_or_update_incident",
            return_value={},
        )

        t2g.sync_open_issues()

        asset = test_vuln.pop("asset")
        plugin = test_vuln.pop("plugin")

        mocked_get_asset_details.assert_called_with(asset["uuid"])
        mocked_create_or_update_incident.assert_called_with(
            plugin, asset, test_vuln, Config.open_incident_labels
        )

    def test_sync_open_no_vulns(
        self,
        mocker: MockFixture,
        dummy_tenable_utils: TenableUtils,
        dummy_gitlab_utils: GitlabUtils,
        mocked_get_asset_details: MagicMock,
    ) -> None:
        t2g = Tenable2Gitlab(dummy_tenable_utils, dummy_gitlab_utils)
        mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.open_vulnerabilities_iterator",
            return_value=[],
        )
        mock_get_asset_details = mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.get_asset_details",
            return_value=[],
        )

        t2g.sync_open_issues()
        mock_get_asset_details.assert_not_called()


class TestSyncFixed:
    def test_sync_fixed(
        self,
        mocker: MockFixture,
        dummy_tenable_utils: TenableUtils,
        dummy_gitlab_utils: GitlabUtils,
        mocked_get_asset_details: MagicMock,
    ) -> None:
        t2g = Tenable2Gitlab(dummy_tenable_utils, dummy_gitlab_utils)
        dummy_vuln = build_dummy_vulnerabity()

        # the original object is altered in runtime.
        # better for efficiency, bad for tsts
        test_vuln = dummy_vuln.copy()

        mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.fixed_vulnerabilities_iterator",
            return_value=[dummy_vuln],
        )

        mocked_close_incident = mocker.patch(
            "tenable_gitlab.utils.gitlab.GitlabUtils.close_incident",
            return_value={},
        )

        mocked_get_asset_details = mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.get_asset_details",
            return_value=[],
        )

        t2g.sync_fixed_issues()

        asset = test_vuln.pop("asset")
        plugin = test_vuln.pop("plugin")

        mocked_get_asset_details.assert_called_once_with(asset["uuid"])
        mocked_close_incident.assert_called_once_with(
            plugin,
            asset,
            test_vuln,
            Config.fixed_closing_reason,
            Config.fixed_incident_labels,
        )

    def test_sync_fixed_no_vulns(
        self,
        mocker: MockFixture,
        dummy_tenable_utils: TenableUtils,
        dummy_gitlab_utils: GitlabUtils,
        mocked_get_asset_details: MagicMock,
    ) -> None:
        t2g = Tenable2Gitlab(dummy_tenable_utils, dummy_gitlab_utils)

        mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.fixed_vulnerabilities_iterator",
            return_value=[],
        )

        mocked_close_incident = mocker.patch(
            "tenable_gitlab.utils.gitlab.GitlabUtils.close_incident",
            return_value={},
        )

        t2g.sync_fixed_issues()

        mocked_get_asset_details.assert_not_called()
        mocked_close_incident.assert_not_called()


class TestSyncRcasted:
    def test_sync_recasted(
        self,
        mocker: MockFixture,
        dummy_tenable_utils: TenableUtils,
        dummy_gitlab_utils: GitlabUtils,
        mocked_get_asset_details: MagicMock,
    ) -> None:
        t2g = Tenable2Gitlab(dummy_tenable_utils, dummy_gitlab_utils)
        dummy_vuln = build_dummy_vulnerabity()
        dummy_vuln["recast_rule_uuid"] = "xx"
        dummy_vuln["recast_reason"] = random_string(10)
        new_severity = "info"
        dummy_vuln["severity"] = new_severity
        dummy_vuln["severity_num"] = Severity[new_severity.upper()].value

        # the original object is altered in runtime.
        # better for efficiency, bad for tsts
        test_vuln = dummy_vuln.copy()

        mocker.patch(
            "tenable_gitlab.utils.tenable.TenableUtils.recasted_vulnerabilities_iterator",
            return_value=[dummy_vuln],
        )

        mocked_close_incident = mocker.patch(
            "tenable_gitlab.utils.gitlab.GitlabUtils.close_incident",
            return_value={},
        )

        t2g.sync_recasted_issues()

        asset = test_vuln.pop("asset")
        plugin = test_vuln.pop("plugin")
        recast_reason = test_vuln["recast_reason"]

        mocked_get_asset_details.assert_called_once_with(asset["uuid"])
        mocked_close_incident.assert_called_once_with(
            plugin,
            asset,
            test_vuln,
            recast_reason,
            Config.recasted_incident_labels,
        )
