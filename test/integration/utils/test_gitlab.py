from test.conftest import random_string
from typing import Any, Dict

from tenable_gitlab.config import Config
from tenable_gitlab.utils.gitlab import GitlabUtils


class TestCreate:
    # Create plugin issue
    def test_create_plugin_issue(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
    ) -> None:
        # Create plugin issue
        res = gitlab_utils.create_or_update_plugin_issue(random_plugin)

        assert str(random_plugin["id"]) in res.title
        assert random_plugin["name"] in res.title

        plugin_issue = gitlab_utils.get_plugin_issue(random_plugin["id"])

        assert plugin_issue is not None

        assert str(random_plugin["id"]) in plugin_issue.title
        assert random_plugin["name"] in plugin_issue.title

        assert random_plugin["description"] in plugin_issue.description

    # Create asset with existing plugin issue
    def test_create_asset_existing_plugin_issue(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, str],
    ) -> None:
        # Create plugin issue
        plugin_issue = gitlab_utils.create_or_update_plugin_issue(
            random_plugin
        )

        assert plugin_issue is not None

        # Create incident
        incident = gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        assert incident is not None

        # TODO: Check for link

    # Create asset with no plugin issue
    def test_create_asset_no_plugin_issue(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, str],
    ) -> None:
        # Create incident
        gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        # Check if plugin issue was created
        get_issue = gitlab_utils.get_plugin_issue(random_plugin["id"])
        assert get_issue is not None
        assert str(random_plugin["id"]) in get_issue.title

    def test_create_asset_default_labels(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, str],
    ) -> None:
        # Create plugin issue
        plugin_issue = gitlab_utils.create_or_update_plugin_issue(
            random_plugin
        )

        assert plugin_issue is not None

        # Create incident
        incident = gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        assert incident is not None
        assert set(Config.default_incident_labels).issubset(incident.labels)

    def test_create_asset_custom_labels(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, str],
    ) -> None:
        # Create plugin issue
        plugin_issue = gitlab_utils.create_or_update_plugin_issue(
            random_plugin
        )
        random_labels = [random_string(10), random_string(5)]

        assert plugin_issue is not None

        # Create incident
        incident = gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability, random_labels
        )

        assert incident is not None
        assert set(random_labels).issubset(incident.labels)

    # Update plugin
    def test_update_plugin(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
    ) -> None:
        # Create plugin issue 1
        gitlab_utils.create_or_update_plugin_issue(random_plugin)

        get_issue_1 = gitlab_utils.get_plugin_issue(random_plugin["id"])
        assert get_issue_1 is not None
        assert str(random_plugin["id"]) in get_issue_1.title

        # Create plugin issue 2
        description_update = "\nUpdated for test_update_plugin.\n"
        random_plugin["description"] += description_update
        gitlab_utils.create_or_update_plugin_issue(random_plugin)

        get_issue_2 = gitlab_utils.get_plugin_issue(random_plugin["id"])
        assert get_issue_2 is not None
        assert str(random_plugin["id"]) in get_issue_2.title
        assert description_update in get_issue_2.description

        # Check if both have the same issue id on gitlab
        assert get_issue_1.get_id() == get_issue_2.get_id()

    # Update asset
    def test_update_asset(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, str],
    ) -> None:
        # Create incident
        gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        plugin_id = random_plugin["id"]
        hostname = random_asset["hostname"][0]

        # Check if incident was created
        get_issue = gitlab_utils.get_asset_incident(plugin_id, hostname)
        assert get_issue is not None
        assert str(hostname) in get_issue.title
        assert random_asset["operating_system"] in get_issue.description
        assert "incident" in get_issue.labels

        # changing something in the asset, to check later
        random_asset["operating_system"] = random_string(10)

        # Update incident
        gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        # Check if incident was created
        get_updated_issue = gitlab_utils.get_asset_incident(
            plugin_id, hostname
        )
        assert get_updated_issue is not None
        assert str(hostname) in get_updated_issue.title
        assert (
            random_asset["operating_system"] in get_updated_issue.description
        )
        assert "incident" in get_updated_issue.labels

    def test_close_incident_existing_plugin_links(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, str],
    ) -> None:
        # Create plugin issue
        plugin_issue = gitlab_utils.create_or_update_plugin_issue(
            random_plugin
        )

        assert plugin_issue is not None

        # Create incident 1
        gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )
        # Create incident 2
        random_asset_2 = random_asset.copy()
        random_asset_2["hostname"][0] = (
            random_asset_2["hostname"][0] + "_updated"
        )
        gitlab_utils.create_or_update_incident(
            random_plugin, random_asset_2, random_vulnerability
        )

        # Close incident 1
        gitlab_utils.close_incident(
            random_plugin,
            random_asset,
            random_vulnerability,
            Config.fixed_closing_reason,
        )

        # Must have 1 link left
        links = gitlab_utils._get_open_vulnerability_links_for_plugin(
            random_plugin["id"]
        )
        assert len(links) == 1
        # Plugin incident should exist and be opened
        plugin = gitlab_utils.get_plugin_issue(random_plugin["id"])
        assert plugin is not None

    def test_close_incident_no_plugin_links(
        self,
        gitlab_utils: GitlabUtils,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, str],
    ) -> None:
        # Create plugin issue
        plugin_issue = gitlab_utils.create_or_update_plugin_issue(
            random_plugin
        )

        assert plugin_issue is not None

        # Create incident
        gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        # Close incident 1
        gitlab_utils.close_incident(
            random_plugin,
            random_asset,
            random_vulnerability,
            Config.fixed_closing_reason,
        )

        # Must have 0 link left
        links = gitlab_utils._get_open_vulnerability_links_for_plugin(
            random_plugin["id"]
        )
        assert len(links) == 0

        # Plugin incident should not exist (should have been closed
        plugin = gitlab_utils.get_plugin_issue(random_plugin["id"])
        assert plugin is None


# TODO: Update asset and plugin
